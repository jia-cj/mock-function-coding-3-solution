  const products = [
      { name: 'Laptop', category: 'Electronics', price: 1000, quantity: 5 },
      { name: 'Shirt', category: 'Clothing', price: 20, quantity: 10 },
      { name: 'Book', category: 'Books', price: 15, quantity: 25 },
      { name: 'Headphones', category: 'Electronics', price: 50, quantity: 8 }
    ];

  function getTotalInventoryValue(products) {
    let totalValue = 0;
      for (const product of products) {
      totalValue += product.price * product.quantity;
    }
    return totalValue;
  }

  const users = [
          { username: 'alice', password: 'pass123' },
          { username: 'bob', password: 'pass456' },
          { username: 'charlie', password: 'pass789' }
        ];

    function isUserValid(users, username, password) {
      for (const user of users) {
      if (user.username === username && user.password === password) {
        return true;
      }
    }
    return false;
  }

  const employees = [
      { name: 'Alice', position: 'Manager', hoursWorked: 40, hourlyRate: 30 },
      { name: 'Bob', position: 'Developer', hoursWorked: 35, hourlyRate: 25 },
      { name: 'Charlie', position: 'Intern', hoursWorked: 20, hourlyRate: 15 }
  ];

  function calculateSalary(employees) {
    for (const employee of employees) {
      employee.totalSalary = employee.hoursWorked * employee.hourlyRate;
    }
    return employees;
  }

  const books = [
      { title: 'Book 1', author: 'Author 1'},
      { title: 'Book 2', author: 'Author 2'},
      { title: 'Book 3', author: 'Author 3'},
      { title: 'Book 4', author: 'Author 4'},
      { title: 'Book 5', author: 'Author 4'}
    ];

  const borrowers = [
      { name: 'Borrower 1', books: ['Book 4'] },
      { name: 'Borrower 2', books: ['Book 5'] }
    ];

    function getAvailableBooks(books, borrowers) {
    const borrowedBooks = new Set();
    for (const borrower of borrowers) {
      for (const book of borrower.books) {
        borrowedBooks.add(book);
      }
    }

    const availableBooks = books.filter((book) => !borrowedBooks.has(book.title));
    return availableBooks;
  }

  const sales = [
    { date: '2023-01-01', product: 'Product A', quantity: 10 },
    { date: '2023-01-01', product: 'Product B', quantity: 5 },
    { date: '2023-01-02', product: 'Product A', quantity: 8 },
    { date: '2023-01-02', product: 'Product C', quantity: 3 }
  ];

  function getTotalSalesByDate(sales, date) {
    let totalQuantity = 0;
    for (const sale of sales) {
      if (sale.date === date) {
        totalQuantity += sale.quantity;
      }
    }
    return totalQuantity;
  }


  module.exports = {
      getTotalInventoryValue,
      isUserValid,
      calculateSalary,
      getAvailableBooks,
      getTotalSalesByDate
  };

